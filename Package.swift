// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "Danger-swift-Demo",
    dependencies: [
      .package(url: "https://github.com/danger/swift.git", from: "3.0.0")
    ]
)
